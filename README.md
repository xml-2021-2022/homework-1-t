## Homework 1 - Traditional ##

For the proposed solution, I came up with a XML data model for a computer hardware store (something like PC Garage for example), that sells laptops, desktops and individual pc components (CPUs for the current implementation).


# Used technologies:

 - For creating the XML document, I used Visual Studio with the RedHat's XML plugin and Josh Johnson's XML Tools plugin.
 - For the parser's code program, I used Java in Eclipse.
 - For the parser, I chose SAX in Java.


# Project structure:

 - CPU.java -> POJO for the CPU object;
 - Desktop.java -> POJO for the Desktop object;
 - HardwareStore.xml -> The XML document that denotes the XML database;
 - Laptop.java -> POJO for the Laptop object;
 - PCHardwareStoreHandler -> Class extended from the SAX Default Handler, used for capturing the events, where I overrided some methods such as startDocument(), startElement(), etc. Here I also wrote the filter class;
 - ReadSaxParser -> The 'main' class, that includes the procedures of initializing a SAXParser object, and parsing the 'HardwareStore.xml' document with our custom handler. Here also lies the command line interface that I created to go along with the filter.


 # For the filter:

  The filter takes the following arguments from the command line interface:
  - CPU
  - GPU
  - Budget

  Then the application finds both the laptops and desktops that share the specified specs and that are under the budget.

    
 
